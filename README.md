# benchmarking_data_retention

This application is made to demonstrate data retention mechanisms while using FLI model on modeling
GPS data.

## Getting Started

This project requires FLI, included in the package temporalbdd.
The temporal data base package is available at:

[https://gitlab.inria.fr/oruas/temporalbddflutter](https://gitlab.inria.fr:oruas/temporalbddflutter)

This project is required.
Clone the project and update the path of the temporalbdd in the pubspec.yaml

#### Cabspotting data

This application uses data from the cabspotting dataset.

You need to export data relative to cabspotting's user0 to a `cabspotting-user0` file, then place this
file in the test phone: 

* On Android, in the `/data/data/fr.inria.temporaldb.benchmarking_data_retention/files/datasets` directory.

## Applications

This repository holds two applications:
* Under `/lib`, a Flutter application allows you to play with data retention mechanisms, and see their effect on data in real-time;
* Under `/tests`, `poi_test.dart` script is used to compile results, to be displayed in the final paper.

#### Desktop

To run experiments, you can use the following command: `flutter test poi_test.dart`.

This will create files in `/results` directory, which will be used by the Python script to compute statistics and create charts.

Once `/results` directory is populated, you can generate charts by running `python3 scripts/graph.py`.

#### Mobile app

The mobile application allows to manipulate epsilon and check the effect on a modeled part of a trace; it also allows to compute
in-situ points of interest with a user-prompted epsilon, and observe the impact on the quality of POIs.

| <img src="docs/models.png" width="250"/> <img src="docs/pois.png" width="250"/>  |
| :---------------: |
| Mobile application screenshots  |