import 'dart:convert';
import 'dart:io';

import 'package:benchmarking_data_retention/utils/duration_strategy.dart';
import 'package:benchmarking_data_retention/utils/updatable_gps_model.dart';
import 'package:temporalbdd/geo_example/poi.dart';
import 'package:temporalbdd/geo_example/utils.dart';
import 'package:temporalbdd/gpsDB.dart';
import 'package:temporalbdd/models/fastLinearInterpolation.dart';
import 'package:temporalbdd/retention/strategy/blind_strategy.dart';
import 'package:temporalbdd/retention/strategy/retention_strategy.dart';
import 'package:tuple/tuple.dart';
import 'package:flutter_test/flutter_test.dart';


Future<void> computeUser0Pois() async {
  late List<Tuple3<double, double, double>> dataset;

  final String path = Directory.current.path;
  String sep = Platform.pathSeparator;
  String datasetPath = "$path${sep}datasets${sep}cabspotting-user0";
  List<Tuple3<double, double, double>> gpsDatasetUser0 = [];
  try {
    gpsDatasetUser0 = readCSVUser(datasetPath, 0);
    print('Successfully loaded GPS dataset (locations count: ${gpsDatasetUser0.length})');
    dataset = gpsDatasetUser0.reversed.toList();
  }
  catch (e) {
    print('Error while loading the dataset: $e');
    print('Experiment on cabspotting: aborted');
    return;
  }

  double epsilon = 0.001;
  double step = 0.1;
  double target = 0.55;

  Directory resultsDir = Directory("$path${sep}results");
  if (!resultsDir.existsSync()) {
    resultsDir.create();
  }

  // Create ground truth without modeling
  List<Tuple3<double, double, double>> data = dataset.toList();
  var poiRaw = POI.getPOI(data, 5, 500, 2);
  File resultsFile = File("$path${sep}results${sep}raw.json");
  await resultsFile.writeAsString(jsonEncode({
    'points': poiRaw.map((e) => [e.item1, e.item2]).toList()
  }));

  for (double err=epsilon; err<target; err+=step) {
    err = double.parse(err.toStringAsFixed(3));
    List<Tuple3<double, double, double>> data = [];
    File resultsFile = File("$path${sep}results$sep$err.json");

    UpdatableGPSModel gpsModel = UpdatableGPSModel();
    // List of timestamps to regenerate the trace
    List<double> timestamps = [];

    // Trace is modeled with an epsilon = 0.001
    for (var point in dataset) {
      gpsModel.add(point.item1, point.item2, point.item3);
      timestamps.add(point.item1);
    }

    // We then recompute models with input epsilon
    // gpsModel.applyStrategy(BlindStrategy(err));
    gpsModel.applyStrategy(DurationStrategy(err));
    data = gpsModel.getTrace(timestamps);

    var poiModeled = POI.getPOI(data, 5, 500, 2);
    print("e=$err, POIs count=${poiModeled.length}");
    await resultsFile.writeAsString(jsonEncode({
      'distanceDistribution': distributionDistancePOI(poiModeled, poiRaw),
      'points': poiModeled.map((e) => [e.item1, e.item2]).toList()
    }));
  }

  print("Exported results to ${resultsDir.path}.");
}

Future<void> computeAllCabspottingPOIs(int usersCount, RetentionStrategy<FastLinearInterpolation> Function(double error) getStrategy) async {
  Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
  final String path = Directory.current.path;
  String sep = Platform.pathSeparator;
  String datasetPath = "$path${sep}datasets${sep}cabspotting";

  // Load entire dataset
  try {
    gpsDataset = readCSV(datasetPath);
    print('Successfully loaded GPS dataset');
  }
  catch (e) {
    print('Error while loading the dataset: $e');
    print('Experiment on cabspotting: aborted');
    return;
  }

  // Create results directory
  Directory resultsDir = Directory("$path${sep}results");
  if (resultsDir.existsSync()) {
    resultsDir.deleteSync(recursive: true);
  }
  await resultsDir.create();

  List<double> epsilons = [0.001, 0.002, 0.005, 0.01, 0.5];

  for (double err in epsilons) {
    err = double.parse(err.toStringAsFixed(3));
    List<double> distances = [];
    RetentionStrategy<FastLinearInterpolation> strategy = getStrategy(err);

    for (var entry in gpsDataset.entries.toList().sublist(0, usersCount)) {
      print("User n°${entry.key}, e=$err");

      // List of timestamps to regenerate traces
      List<double> timestamps = [];

      // Model user trace
      GPSDB rawGpsModel = GPSDB();
      UpdatableGPSModel gpsModel = UpdatableGPSModel();
      
      for (var point in entry.value.reversed) {
        rawGpsModel.add(point.item1, point.item2, point.item3);
        gpsModel.add(point.item1, point.item2, point.item3);
        timestamps.add(point.item1);
      }

      var poiRaw = POI.getPOI(rawGpsModel.getTrace(timestamps), 5, 500, 2);

      // We then recompute models with input epsilon
      gpsModel.applyStrategy(strategy);
      List<Tuple3<double, double, double>> modeledTrace = gpsModel.getTrace(timestamps);
      var poiModeled = POI.getPOI(modeledTrace, 5, 500, 2);
      distances.addAll(distributionDistancePOI(poiModeled, poiRaw));
    }

    File resultsFile = File("$path${sep}results$sep$err.json");
    await resultsFile.writeAsString(jsonEncode({
      'distanceDistribution': distances
    }));
  }
}

void main() async {
  test('Run computation', () async {
    // await computeUser0Pois();
    await computeAllCabspottingPOIs(5, (double err) => BlindStrategy(err));
  }, timeout: const Timeout(Duration(days: 7)));
}