import json
import numpy as np
import math
import random
import csv
from statistics import median

import matplotlib.pyplot as plt
import matplotlib.patches as pat
import os, sys

class Experiment:
    def __init__(self, e, absolute_path):
        self.epsilon = e

        f = open(absolute_path)
        data = json.load(f)
        self.distanceDistributions = data['distanceDistribution']

def printPOIsGraph():
    cwd = os.getcwd()
    resultsDir = cwd + os.sep + "results"
    experiments = []

    for file in sorted(os.listdir(resultsDir)):
        filename = os.fsdecode(file)
        if filename.endswith(".json") and filename != "raw.json":
            absolute_name = os.path.join(resultsDir, filename)
            experiments.append( Experiment(float(filename[:-5]), absolute_name) )

    plt.clf()

    # Draw CDFs
    for exp in experiments:
        data = exp.distanceDistributions
        count = len(data)
        # sort the data in ascending order
        x = np.sort(data)
        # get the cdf values of y
        y = np.arange(count) / float(count)
        plt.plot(x, y, linestyle='-', label="ε=" + str(exp.epsilon))

    # Zoom in a bit
    plt.axis([0, 50000, 0.6, 1])

    plt.title("Cabspotting users' POIs distance to raw counterparts")
    plt.xlabel('Distance to raw POI (m)')
    plt.legend()
    plt.savefig('graph.pdf', transparent = True)


printPOIsGraph()
