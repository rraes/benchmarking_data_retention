import 'package:temporalbdd/models/fastLinearInterpolation.dart';
import 'package:temporalbdd/retention/strategy/retention_strategy.dart';
import 'package:tuple/tuple.dart';

class DurationStrategy extends RetentionStrategy<FastLinearInterpolation> {
  final double error;
  DurationStrategy(this.error);

  @override
  FastLinearInterpolation clean(FastLinearInterpolation model) {
    List<double> modelsTimestamps = model.getTimestamps();
    List<double> modelsDurations = [];

    // Compute models duration
    for (int i=0; i<modelsTimestamps.length-1; i++) {
      double modelDuration = modelsTimestamps[i+1] - modelsTimestamps[i];
      modelsDurations.add(modelDuration);
    }

    // Compute duration threshold
    double durationThreshold = 0;
    modelsDurations.sort();
    int middle = modelsDurations.length ~/ 2;
    if (modelsDurations.length % 2 == 1) {
      durationThreshold = modelsDurations[middle];
    } else {
      durationThreshold = ((modelsDurations[middle - 1] + modelsDurations[middle]) / 2.0);
    }

    // Rewrite values in new model with a bigger error
    double originalEpsilon = model.getError();
    FastLinearInterpolation rougherModel = FastLinearInterpolation();
    for (int i=0; i<modelsTimestamps.length-1; i++) {
      bool degradeEpsilon = modelsDurations[i] < durationThreshold;
      rougherModel.setParameters({'error': degradeEpsilon ? error.toString() : originalEpsilon.toString()});
      for (double t=modelsTimestamps[i]; t<modelsTimestamps[i+1]; t++) {
        rougherModel.add(t, model.read(t));
      }
    }

    List<Tuple3<double, double, double>> newModels = rougherModel.getRawModels();
    model.setRawModels(newModels);
    return model;
  }
}