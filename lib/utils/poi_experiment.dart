import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:tuple/tuple.dart';

class POIExperiment {
  final double epsilon;
  final List<Tuple3<double, double, double>> data;
  final Color color;
  late List<LatLng> results;
  final Stopwatch _stopwatch = Stopwatch();
  late bool show;

  POIExperiment(this.epsilon, this.data, this.color) {
    results = [];
    show = true;
  }

  void start() {
    _stopwatch.start();
  }

  void stop(List<LatLng> results) {
    this.results = results;
    _stopwatch.stop();
  }

  bool get isRunning {
    return _stopwatch.isRunning;
  }

  String get duration {
    return _stopwatch.elapsed.toString().split('.').first.padLeft(8, "0");
  }
}