import 'package:temporalbdd/gpsDB.dart';
import 'package:temporalbdd/models/fastLinearInterpolation.dart';
import 'package:temporalbdd/retention/strategy/retention_strategy.dart';

/// This GPSDB implementation types underlying latitude and longitude models as
/// FastLinearInterpolation models, and allows to apply data retention
/// strategies on both of them.
class UpdatableGPSModel extends GPSDB {
  late final FastLinearInterpolation _latM;
  late final FastLinearInterpolation _lngM;

  UpdatableGPSModel() {
    _latM = FastLinearInterpolation();
    _lngM = FastLinearInterpolation();
  }

  void applyStrategy(RetentionStrategy<FastLinearInterpolation> strategy) {
    strategy.clean(latM);
    strategy.clean(lngM);
  }

  @override
  FastLinearInterpolation get latM {
    return _latM;
  }

  @override
  FastLinearInterpolation get lngM {
    return _lngM;
  }
}