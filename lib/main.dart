import 'dart:io';

import 'package:benchmarking_data_retention/views/chart_view.dart';
import 'package:benchmarking_data_retention/views/map_view.dart';
import 'package:flutter/material.dart';
import 'package:temporalbdd/geo_example/utils.dart';
import 'package:temporalbdd/platform.dart';
import 'package:tuple/tuple.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Tuple3<double, double, double>> _dataset = [];

  @override
  void initState() {
    super.initState();
    _loadCabspotting();
  }

  Future<void> _loadCabspotting() async {
    final path = await localPath;
    String sep = Platform.pathSeparator;
    String datasetPath = "$path${sep}datasets${sep}cabspotting-user0";
    List<Tuple3<double, double, double>> gpsDatasetUser0 = [];
    try {
      gpsDatasetUser0 = readCSVUser(datasetPath, 0);
      debugPrint('Successfully loaded GPS dataset (locations count: ${gpsDatasetUser0.length})');
      setState(() {
        _dataset = gpsDatasetUser0.reversed.toList();
      });
    }
    catch (e) {
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on cabspotting: aborted');
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              bottom: const TabBar(
                tabs: [
                  Tab(icon: Icon(Icons.area_chart_outlined)),
                  Tab(icon: Icon(Icons.map_outlined)),
                ],
              ),
              title: const Text('Benchmarking data retention'),
            ),
            body: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              children: [
                ChartView(dataset: _dataset.isEmpty ? _dataset : _dataset.sublist(0, 100)),
                MapView(dataset: _dataset),
              ],
            ),
          ),
        )
    );
  }
}