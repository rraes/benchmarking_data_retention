import 'package:benchmarking_data_retention/utils/poi_experiment.dart';
import 'package:benchmarking_data_retention/utils/updatable_gps_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:temporalbdd/geo_example/poi.dart';
import 'package:temporalbdd/retention/strategy/blind_strategy.dart';
import 'package:tuple/tuple.dart';

class MapView extends StatefulWidget {
  final List<Tuple3<double, double, double>> dataset;
  const MapView({super.key, required this.dataset});
  @override
  State<StatefulWidget> createState() => _MapViewState();
}

// This must be a top-level method to be executed in an isolate.
// https://api.flutter.dev/flutter/foundation/compute-constant.html
List<LatLng> _computeRawPOIs(POIExperiment experiment) {
  List<Tuple3<double, double, double>> data = [];

  if (experiment.epsilon == 0) {
    data = experiment.data;
  } else {
    UpdatableGPSModel gpsModel = UpdatableGPSModel();
    // List of timestamps to regenerate the trace
    List<double> timestamps = [];

    // Trace is modeled with an epsilon = 0.001
    for (var point in experiment.data) {
      gpsModel.add(point.item1, point.item2, point.item3);
      timestamps.add(point.item1);
    }

    // We then recompute models with input epsilon
    gpsModel.applyStrategy(BlindStrategy(experiment.epsilon));

    data = gpsModel.getTrace(timestamps);
  }

  var poiRaw = POI.getPOI(data, 5, 500, 2);
  return poiRaw.map((e) => LatLng(e.item1, e.item2)).toList();
}

class _MapViewState extends State<MapView> {
  List<POIExperiment> experiments = [];

  @override
  void initState() {
    super.initState();
    experiments.add(
      POIExperiment(0, widget.dataset, Colors.red),
    );
  }

  void _startExperiment(POIExperiment exp) {
    setState(() {
      exp.start();
    });
    compute(_computeRawPOIs, exp)
        .then((value) {
      setState(() {
        exp.stop(value);
      });
    });
  }

  Widget _getExperimentationTrailingWidget(POIExperiment exp) {
    if (exp.isRunning) {
      return Container(
        width: 30,
        height: 30,
        margin: const EdgeInsets.only(top: 10, right: 10),
        child: const CircularProgressIndicator()
      );
    } else if (exp.results.isNotEmpty) {
      return Container(
        margin: const EdgeInsets.only(top: 10),
        child: Text("Done in\n${exp.duration}"),
      );
    }

    return IconButton(
      icon: const Icon(Icons.play_arrow),
      onPressed: () => _startExperiment(exp),
    );
  }

  Widget _getExperimentationSubtitles(POIExperiment exp) {
    String poisCountText = "POIs count: ${exp.results.length}";
    String subtext = exp.isRunning
        ? "Running..."
        : exp.results.isEmpty
          ? 'Tap tile to start the experiment.'
          : "Tap tile to toggle results display.";
    return Text("$poisCountText\n$subtext");
  }


  @override
  Widget build(BuildContext context) {
    List<Widget> layers = [
      TileLayer(
        urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        userAgentPackageName: 'com.example.app',
      )
    ];
    List<Widget> markerLayers = experiments
        .where((element) => element.show)
        .map((exp) {
          return MarkerLayer(
            markers: exp.results.map((point) {
              return Marker(
                point: point,
                builder: (context) => Icon(Icons.location_on, color: exp.color)
              );
            }).toList(),
          );
        }).toList();
    layers.addAll(markerLayers);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              String value = "";

              return AlertDialog(
                title: const Text('Create experiment'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      const Text('Enter epsilon of new experiment.'),
                      TextField(
                        keyboardType: TextInputType.number,
                        maxLines: 1,
                        onChanged: (v) {
                          value = v;
                        },
                      )
                    ],
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Create'),
                    onPressed: () {
                      double cleanedValue;
                      try {
                        cleanedValue = double.parse(value);
                        if (cleanedValue.isNegative) {
                          return;
                        }
                      } catch (err) {
                        return;
                      }
                      setState(() {
                        experiments.add(
                            POIExperiment(cleanedValue, widget.dataset,
                                Colors.green)
                        );
                      });
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        },
        child: const Icon(Icons.add),
      ),

      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: FlutterMap(
              options: MapOptions(
                center: LatLng(37.7280484,-122.4404873),
                zoom: 11,
              ),
              children: layers,
            ),
          ),

          Expanded(
            flex: 2,
            child: ListView(
              children: experiments.map((e) {
                return ListTile(
                  title: Text("Epsilon: ${e.epsilon}"),
                  subtitle: _getExperimentationSubtitles(e),
                  isThreeLine: true,
                  minLeadingWidth: 20,
                  leading: Container(
                    width: 10,
                    margin: const EdgeInsets.only(top: 10, right: 15),
                    child: Icon(
                      Icons.location_on,
                      color: e.show ? e.color : Colors.grey.shade600,
                    ),
                  ),
                  trailing: _getExperimentationTrailingWidget(e),
                  onTap: () {
                    if (e.results.isEmpty) {
                      if (!e.isRunning) {
                        _startExperiment(e);
                      }
                      return;
                    }
                    setState(() {
                      e.show = !e.show;
                    });
                  },
                  tileColor: e.show ? Colors.grey.shade50 : Colors.grey.shade300,
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
