import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:temporalbdd/models/fastLinearInterpolation.dart';
import 'package:temporalbdd/retention/strategy/blind_strategy.dart';
import 'package:tuple/tuple.dart';

class ChartView extends StatefulWidget {
  final List<Tuple3<double, double, double>> dataset;
  const ChartView({super.key, required this.dataset});
  @override
  State<StatefulWidget> createState() => _ChartViewState();
}

class _ChartViewState extends State<ChartView> {
  FastLinearInterpolation _model = FastLinearInterpolation();
  double _epsilon = 0.001;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_model.getNbModels() == 1) {
      _loadUpModel();
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _getChart(),
          _getStats(),
        ],
      ),
    );
  }

  Widget _getChart() {
    if (widget.dataset.isEmpty) {
      return const Text("Nothing to see here.");
    }

    return AspectRatio(
      aspectRatio: 1,
      child: LineChart(
        LineChartData(
          lineBarsData: [
            LineChartBarData(
                dotData: FlDotData(show: false),
                spots: widget.dataset.map((e) => FlSpot(e.item1, e.item3)).toList(),
                color: Colors.red
            ),
            LineChartBarData(
              dotData: FlDotData(show: false),
              spots: _getModelPoints(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getStats() {
    int modelsCount = _model.getNbModels();

    return
      Container(
        margin: const EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("${widget.dataset.length} dataset entries"),
            Text("$modelsCount models (${3*modelsCount} bytes)"),
            Container(
              margin: const EdgeInsets.only(top: 10),
              child: Column(
                children: [
                  Text("Model epsilon: $_epsilon"),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          _epsilon += 0.001;
                          BlindStrategy strategy = BlindStrategy(_epsilon);
                          setState(() {
                            _model = strategy.clean(_model);
                          });
                        },
                        child: const Text("Increase model epsilon"),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _loadUpModel();
                            });
                          },
                          child: const Text("Reset model epsilon")
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      );
  }

  void _loadUpModel() {
    _model = FastLinearInterpolation();
    _model.setParameters({'error': '0.001'});
    _epsilon = 0.001;

    for (Tuple3 location in widget.dataset) {
      _model.add(location.item1, location.item3);
    }
  }

  List<FlSpot> _getModelPoints() {
    double startTimestamp = widget.dataset.first.item1;
    double endTimestamp = widget.dataset.last.item1;
    List<FlSpot> spots = [];

    // Read as many points as in the original dataset
    double step = (endTimestamp - startTimestamp) / widget.dataset.length;
    for (double i=startTimestamp; i<endTimestamp; i += step) {
      spots.add(FlSpot(i, _model.read(i)));
    }

    return spots;
  }
}
